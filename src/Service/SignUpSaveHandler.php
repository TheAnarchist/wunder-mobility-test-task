<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\BillingSignUpEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SignUpSaveHandler
{
    public const PAYMENT_ID_KEY = 'paymentDataId';

    private HttpClientInterface $client;

    private string $billingServiceUrl;

    public function __construct(
        HttpClientInterface $client,
        string $billingServiceUrl
    ) {
        $this->client = $client;
        $this->billingServiceUrl = $billingServiceUrl;
    }

    public function retrievePaymentId(BillingSignUpEntity $entity)
    {
        $response = $this->client->request(
            'POST',
            $this->billingServiceUrl,
            [
                'json' => [
                    'customerId' => 1,
                    'owner' => $entity->getOwner(),
                    'iban' => $entity->getIban(),
                ],
            ]
        );

        if ($response->getStatusCode() === Response::HTTP_OK) {
            $response = json_decode($response->getContent(), true);

            return $response[self::PAYMENT_ID_KEY];
        }

        return null;
    }
}